from canto.extra import *
import os
conf_dir = os.environ.get('HOME') + '/.canto'
import sys
sys.path.append(conf_dir)

#source_opml(conf_dir + "/feeds.opml")
add("http://news.ycombinator.com/rss")


###############################################################################
## feed bayesian clasifier
#
#  In this hooks when we talk about tags we are talkin about item tags not
#  feed tags (the ones that are use in the canto manual).

# names of the feeds to use in the parser
classify_feeds = ['Hacker News']
# [(tag, learn_key_binding)]
classify_tags = [("good", "N"), ("bad", "X")]


from crm import Classifier
classifier = Classifier(conf_dir + '/crm', [t for (t, b) in classify_tags])


def classify_hook(canto, item, none):
    from parse import extractTextFromUrl
    global classify_feeds
    global classifier
    #import pdb; pdb.set_trace()
    #print item['link'] #TODO
    if canto['feed']['title'] in classify_feeds:
        text = extractTextFromUrl(item['link'])
        (classification, probability) = classifier.classify(text)
        item['classification'] = classification
        item['probability'] = probability
        return item

new_hook = classify_hook


def learn_tag(tag):
    def learn(obj):
        from canto.gui import Gui
        from canto.reader import Reader
        from parse import extractTextFromUrl
        global classify_tags
        global classifier
        summary = ""
        if isinstance(obj, Gui):
            item = obj.sel['item']
        elif isinstance(obj, Reader):
            item = obj.story
        text = extractTextFromUrl(item['link'])
        classifier.learn(tag, text)
        item['classification'] = tag
        item['probability'] = 1
    return learn

for (tag, binding) in classify_tags:
    keys[binding] = learn_tag(tag)
    reader_keys[binding] = learn_tag(tag)


def print_tag(d):
    story = d["story"]
    if 'classification' in story:
        d["content"] += '%b (' + story["classification"] + ')'

r = get_default_renderer()
#add_hook_pre_story(r, print_tag) FIXME: eats the CPU


def show_tag(classification):
    from canto.cfg.filters import Filter
    class show(Filter):
        def __str__(self):
            return "Show " + classification

        def __call__(self, tag, item):
            if 'classification' in item:
                return item['classification'] == classification
            else:
                return False
    return show

##
###############################################################################


class myFilter(Filter):
    def __str__(self):
        return "Show unread good"

    def __call__(self, tag, item):
        if item.was("read"):
            return False
        elif 'classification' in item:
            return item['classification'] == "good"
        else:
            return True

filters = [myFilter, show_tag("bad"), None]


# Configuration of handlers
if os.getenv("TERM") == "linux":
    link_handler("w3m \"%u\"", text=True)       # Text-only
else:
    link_handler("firefox \"%u\"")              # X terminal
link_handler("xpdf \"%u\"", ext="pdf", fetch=True)
image_handler("geeqie \"%u\"", text=True, fetch=True)
