# -*- coding: utf-8 -*-
import re
import urllib
import BeautifulSoup


READREGEXP = {
        # most of these patterns are taken shamelessly from readability(tm).
        # note this is a first an rough iteration, we're only stripping off
        # things that obviously are not what we are looking for as the article meat.
        'unlikelyCandidates': re.compile('combx|comment|community|disqus|extra|foot|header|menu|remark|rss|shoutbox|sidebar|sponsor|ad-break|agegate|pagination|pager|popup|tweet|twitter|facebook|references|navbar|skiplinks|socialnetwork|email|subnav|tagcloud|related', re.I),
        'nextLink': re.compile('(next|weiter|continue|>([^\|]|$)|Â»([^\|]|$))', re.I), # // Match: next, continue,...
        'okMaybeItsACandidate':  re.compile('and|article|body|column|main|shadow'),
        'positive': re.compile('article|body|content|entry|hentry|main|page|pagination|post|text|blog|story'),
        'negative': re.compile('combx|comment|com-|contact|foot|footer|footnote|masthead|media|meta|outbrain|promo|related|scroll|shoutbox|sidebar|sponsor|shopping|tags|tool|widget'),
        'extraneous': re.compile('print|archive|comment|discuss|e[\-]?mail|share|reply|all|login|sign|single'),
        'divToPElements': re.compile('<(a|blockquote|dl|div|img|ol|p|pre|table|ul)'),
        'replaceBrs':            '(<br[^>]*>[ \n\r\t]*){2,}',
        'replaceFonts':          '<(\/?)font[^>]*>',
        'trim':                  '^\s+|\s+$',
        'normalize':             '\s{2,}',
        'killBreaks':            '(<br\s*\/?>(\s|&nbsp;?)*){1,}',
        'videos':                'http:\/\/(www\.)?(youtube|vimeo)\.com',
        'skipFootnoteLink':      '^\s*(\[?[a-z0-9]{1,2}\]?|^|edit|citation needed)\s*$',
        'prevLink':              '(prev|earl|old|new|<|Â«)'
    }


def extractTextFromUrl(url):
    """
    input: string : url (obvious)
    output: string : 'parsed' text
    """
    page = urllib.urlopen(url).read()
    pattern = "<script.*?</script>"
    regex = re.compile(pattern, re.IGNORECASE | re.DOTALL)
    page = regex.sub("", page)
    soup = BeautifulSoup.BeautifulSoup(page)

    body = soup.body
    [x.extract() for x in body.findAll(id=READREGEXP['unlikelyCandidates'])]
    [x.extract() for x in body.findAll(attrs={'class': READREGEXP['unlikelyCandidates']})]
    [x.extract() for x in body.findAll(attrs={'name': READREGEXP['unlikelyCandidates']})]
    [x.extract() for x in body.findAll(attrs={'class': READREGEXP['nextLink']})]

    comments = body.findAll(text=lambda text:isinstance(text, BeautifulSoup.Comment))
    [x.extract() for x in comments]

    return ''.join(body.findAll(text=True))

if __name__=="__main__":
    import sys
    url = sys.argv[1]
    print extractTextFromUrl(url)
